using System;
using NUnit.Framework;
using LibMjcs;

namespace Test
{
	[TestFixture ()]
	public class PatternTest
	{
		[Test ()]
		public void LookingUpToiTu ()
		{
			Hai[] h = null;
			ToiTu k = null;

			h = HaiUtil.Instanciate ("P1112233ToTo");
			k = FormUtil.LookupToiTu (h);
			Assert.AreEqual (new Su (1, Su.SuType.Pin), k.GetHai (0).Label);
			Assert.AreEqual (new Su (1, Su.SuType.Pin), k.GetHai (1).Label);

			k = FormUtil.LookupToiTu (h);
			Assert.AreEqual (new Su (2, Su.SuType.Pin), k.GetHai (0).Label);
			Assert.AreEqual (new Su (2, Su.SuType.Pin), k.GetHai (1).Label);

			k = FormUtil.LookupToiTu (h);
			Assert.AreEqual (new Su (3, Su.SuType.Pin), k.GetHai (0).Label);
			Assert.AreEqual (new Su (3, Su.SuType.Pin), k.GetHai (1).Label);

			k = FormUtil.LookupToiTu (h);
			Assert.AreEqual (new Tu (Tu.TuType.Ton), k.GetHai (0).Label);
			Assert.AreEqual (new Tu (Tu.TuType.Ton), k.GetHai (1).Label);

			Assert.AreEqual ("P1", HaiUtil.ToString (h));
		}

		[Test ()]
		public void LookingUpKoTu ()
		{
			Hai[] h = null;
			KoTu k = null;

			h = HaiUtil.Instanciate ("NaP1112233ToToToTo");
			k = FormUtil.LookupKoTu (h);
			Assert.AreEqual (new Su (1, Su.SuType.Pin), k.GetHai (0).Label);
			Assert.AreEqual (new Su (1, Su.SuType.Pin), k.GetHai (1).Label);
			Assert.AreEqual (new Su (1, Su.SuType.Pin), k.GetHai (2).Label);

			k = FormUtil.LookupKoTu (h);
			Assert.AreEqual (new Tu (Tu.TuType.Ton), k.GetHai (0).Label);
			Assert.AreEqual (new Tu (Tu.TuType.Ton), k.GetHai (1).Label);
			Assert.AreEqual (new Tu (Tu.TuType.Ton), k.GetHai (2).Label);

			Assert.AreEqual ("P2P2P3P3ToNa", HaiUtil.ToString (h));
		}

		[Test ()]
		public void LookingUpSyunTu ()
		{
			Hai[] h = null;
			SyunTu k = null;

			h = HaiUtil.Instanciate ("P11122334");
			k = FormUtil.LookupSyunTu (h);
			Assert.AreEqual (new Su (1, Su.SuType.Pin), k.GetHai (0).Label);
			Assert.AreEqual (new Su (2, Su.SuType.Pin), k.GetHai (1).Label);
			Assert.AreEqual (new Su (3, Su.SuType.Pin), k.GetHai (2).Label);

			k = FormUtil.LookupSyunTu (h);
			Assert.AreEqual (new Su (1, Su.SuType.Pin), k.GetHai (0).Label);
			Assert.AreEqual (new Su (2, Su.SuType.Pin), k.GetHai (1).Label);
			Assert.AreEqual (new Su (3, Su.SuType.Pin), k.GetHai (2).Label);

			Assert.AreEqual ("P1P4", HaiUtil.ToString (h));
		}
	}
}

