using System;
using NUnit.Framework;
using LibMjcs;

namespace Test
{
	[TestFixture ()]
	public class HaiLabelTest
	{
		[Test ()]
		public void ToStringTest ()
		{
			Assert.AreEqual ("S1", new Su (1, Su.SuType.Sou).ToString ());
			Assert.AreEqual ("P9", new Su (9, Su.SuType.Pin).ToString ());
			Assert.AreEqual ("W5", new Su (5, Su.SuType.Wan).ToString ());

			Assert.AreEqual ("To", new Tu (Tu.TuType.Ton).ToString ());
			Assert.AreEqual ("Na", new Tu (Tu.TuType.Nan).ToString ());
			Assert.AreEqual ("Sy", new Tu (Tu.TuType.Sya).ToString ());
			Assert.AreEqual ("Pe", new Tu (Tu.TuType.Pei).ToString ());

			Assert.AreEqual ("Ha", new Tu (Tu.TuType.Haku).ToString ());
			Assert.AreEqual ("Ba", new Tu (Tu.TuType.Batu).ToString ());
			Assert.AreEqual ("Ty", new Tu (Tu.TuType.Tyun).ToString ());
		}

		[Test()]
		public void AreSequent ()
		{
			Assert.IsTrue (HaiLabelUtil.AreSequent (
				new Su (3, Su.SuType.Pin),
				new Su (2, Su.SuType.Pin),
				new Su (1, Su.SuType.Pin))
			);

			Assert.IsTrue (HaiLabelUtil.AreSequent (
				new Su (7, Su.SuType.Wan),
				new Su (8, Su.SuType.Wan),
				new Su (9, Su.SuType.Wan))
			);

			Assert.IsFalse (HaiLabelUtil.AreSequent (
				new Su (7, Su.SuType.Sou),
				new Su (8, Su.SuType.Pin),
				new Su (9, Su.SuType.Pin))
			);

			Assert.IsFalse (HaiLabelUtil.AreSequent (
				new Su (1, Su.SuType.Sou),
				new Su (3, Su.SuType.Sou),
				new Su (4, Su.SuType.Sou))
			);

			Assert.IsFalse (HaiLabelUtil.AreSequent (
				new Su (7, Su.SuType.Pin),
				new Su (8, Su.SuType.Pin),
				new Su (9, Su.SuType.Pin),
				new Tu (Tu.TuType.Batu))
			);

			Assert.IsFalse (HaiLabelUtil.AreSequent (
				new Su (1, Su.SuType.Sou),
				new Su (1, Su.SuType.Sou),
				new Su (1, Su.SuType.Sou))
			);
		}
	}
}

