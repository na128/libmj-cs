using System;
using NUnit.Framework;
using LibMjcs;

namespace Test
{
	[TestFixture()]
	public class HaiTest
	{
		[Test ()]
		public void Equality ()
		{
			Assert.AreEqual ((new Su (1, Su.SuType.Pin)), (new Su (1, Su.SuType.Pin)));
			Assert.AreEqual ((new Tu (Tu.TuType.Batu)), (new Tu (Tu.TuType.Batu)));

			Assert.AreNotEqual ((new Su (1, Su.SuType.Pin)), (new Su (2, Su.SuType.Pin)));
			Assert.AreNotEqual ((new Su (9, Su.SuType.Pin)), (new Su (8, Su.SuType.Pin)));
			Assert.AreNotEqual ((new Su (1, Su.SuType.Sou)), (new Su (1, Su.SuType.Pin)));
			Assert.AreNotEqual ((new Su (1, Su.SuType.Wan)), (new Su (1, Su.SuType.Pin)));

			Assert.AreNotEqual ((new Tu (Tu.TuType.Batu)), (new Tu (Tu.TuType.Haku)));
			Assert.AreNotEqual ((new Tu (Tu.TuType.Tyun)), (new Tu (Tu.TuType.Nan)));
			Assert.AreNotEqual ((new Tu (Tu.TuType.Ton)), (new Tu (Tu.TuType.Batu)));
			Assert.AreNotEqual ((new Tu (Tu.TuType.Haku)), (new Tu (Tu.TuType.Sya)));
			Assert.AreNotEqual ((new Tu (Tu.TuType.Pei)), (new Tu (Tu.TuType.Tyun)));
		}

		[Test ()]
		public void UtilTest ()
		{
			Assert.IsTrue (new Hai (new Su (1, Su.SuType.Pin)).IsYaoTyu ());
			Assert.IsTrue (new Hai (new Su (9, Su.SuType.Wan)).IsYaoTyu ());
			Assert.IsTrue (new Hai (new Su (1, Su.SuType.Sou)).IsYaoTyu ());
			Assert.IsTrue (new Hai (new Su (9, Su.SuType.Sou)).IsYaoTyu ());
			Assert.IsFalse (new Hai (new Su (2, Su.SuType.Sou)).IsYaoTyu ());
			Assert.IsFalse (new Hai (new Su (8, Su.SuType.Sou)).IsYaoTyu ());
			Assert.IsFalse (new Hai (new Su (2, Su.SuType.Sou)).IsYaoTyu ());
			Assert.IsFalse (new Hai (new Su (8, Su.SuType.Sou)).IsYaoTyu ());

			Assert.IsTrue (new Hai (new Tu (Tu.TuType.Haku)).IsSanGen ());
			Assert.IsTrue (new Hai (new Tu (Tu.TuType.Batu)).IsSanGen ());
			Assert.IsTrue (new Hai (new Tu (Tu.TuType.Tyun)).IsSanGen ());
			Assert.IsFalse (new Hai (new Tu (Tu.TuType.Haku)).IsFu ());
			Assert.IsFalse (new Hai (new Tu (Tu.TuType.Batu)).IsFu ());
			Assert.IsFalse (new Hai (new Tu (Tu.TuType.Tyun)).IsFu ());

			Assert.IsTrue (new Hai (new Tu (Tu.TuType.Ton)).IsFu ());
			Assert.IsTrue (new Hai (new Tu (Tu.TuType.Nan)).IsFu ());
			Assert.IsTrue (new Hai (new Tu (Tu.TuType.Sya)).IsFu ());
			Assert.IsTrue (new Hai (new Tu (Tu.TuType.Pei)).IsFu ());
			Assert.IsFalse (new Hai (new Tu (Tu.TuType.Ton)).IsSanGen ());
			Assert.IsFalse (new Hai (new Tu (Tu.TuType.Nan)).IsSanGen ());
			Assert.IsFalse (new Hai (new Tu (Tu.TuType.Sya)).IsSanGen ());
			Assert.IsFalse (new Hai (new Tu (Tu.TuType.Pei)).IsSanGen ());
		}

		[Test ()]
		public void Sort ()
		{
			Assert.AreEqual ("P1P2P3S9W1ToNaHaTy", HaiUtil.ToString (HaiUtil.Instanciate ("ToW1NaP321HaS9Ty")));
		}
	}
}

