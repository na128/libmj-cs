using System;
using System.Collections.Generic;

namespace LibMjcs
{
	static class HaiName
	{
		public static String[] TuName = {
			"To", "Na", "Sy", "Pe", "Ha", "Ba", "Ty"
		},

			SuTypeName = {
			"P", "S", "W"
		},

			SuNumber = {
			"1", "2", "3", "4", "5", "6", "7", "8", "9"
		};
	}

	public class HaiLabelUtil
	{
		public static int Compare (IHaiLabel x, IHaiLabel y)
		{
			if (x is Su)
			if (y is Su)
				return ((Su)x).CompareTo ((Su)y);
			else // if (y is Tu)
				return -1;
			else // if (x is Tu)
			if (y is Tu)
				return ((Tu)x).CompareTo ((Tu)y);
			else // if (y is Su)
				return 1;
			//	throw new InvalidProgramException ();
		}

		public static bool AreSequent (params IHaiLabel[] labels)
		{
			Array.Sort (labels);
			Su? last = null;
			foreach (IHaiLabel h in labels) {
				if (!(h is Su)) {
					return false;
				}

				Su s = (Su)h;
				if (last.HasValue && !(
						last.Value.Type == s.Type && 
					last.Value.Number + 1 == s.Number
					)
				) {
					return false;
				}
				last = s;
				
			}
			return true;
		}
	}

	public interface IHaiLabel : IEquatable<IHaiLabel>, IComparable<IHaiLabel>
	{
	}

	[Serializable ()]
	public struct Su : IHaiLabel, IComparable<Su>
	{
		public enum SuType
		{
			Pin = 0,
			Sou = 1,
			Wan = 2,
		}

		private readonly int number;
		private readonly SuType type;

		public int Number {
			get { return number; }
		}

		public SuType Type {
			get { return type; }
		}

		public Su (int number, SuType type)
		{
			this.number = number;
			this.type = type;

			if (!IsValid ())
				throw new InvalidOperationException (this.GetType () + ": Invalid argument");
		}

		private bool IsValid ()
		{
			return number < 10 && number != 0;
		}

		public bool Equals (IHaiLabel other)
		{
			if (!(other is Su))
				return false;
			var su = (Su)other;
			return Number == su.Number && Type == su.Type;
		}

		public int CompareTo (Su other)
		{
			if (Type == other.Type)
				return Number - other.Number;
			return (int)Type - (int)other.Type;
		}

		#region IComparable implementation
		public int CompareTo (IHaiLabel other)
		{
			return HaiLabelUtil.Compare (this, other);
		}
		#endregion

		override public string ToString ()
		{
			return HaiName.SuTypeName [(uint)type] + HaiName.SuNumber [number - 1];
		}
	}

	[Serializable ()]
	public struct Tu : IHaiLabel, IComparable<Tu>
	{
		public enum TuType
		{
			Ton = 0,
			Nan = 1,
			Sya = 2,
			Pei = 3,
			Haku = 4,
			Batu = 5,
			Tyun = 6,
		}

		private readonly TuType type;

		public TuType Type {
			get { return type; }
		}

		public Tu (TuType type)
		{
			this.type = type;
		}

		public bool Equals (IHaiLabel other)
		{
			if (!(other is Tu))
				return false;
			var tu = (Tu)other;
			return Type == tu.Type;
		}

		public int CompareTo (Tu other)
		{
			return (int)Type - (int)other.Type;
		}

		#region IComparable implementation
		public int CompareTo (IHaiLabel other)
		{
			return HaiLabelUtil.Compare (this, other);
		}
		#endregion

		override public string ToString ()
		{
			return HaiName.TuName [(uint)type];
		}
	}
}

