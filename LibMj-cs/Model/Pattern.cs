using System;
using System.Linq;
using System.Collections;

namespace LibMjcs
{
	public static class FormUtil
	{
		public static ToiTu LookupToiTu (Hai[] sorted)
		{
			int i = 0;
			while (++i < sorted.Length) {
				if (sorted [i - 1] != null &&
					sorted [i] != null && 
					sorted [i - 1].Label.Equals (sorted [i].Label)) {
					var r = new ToiTu (sorted [i - 1], sorted [i]);
					sorted [i - 1] = sorted [i] = null;
					return r;
				}
			}
			return null;
		}

		public static KoTu LookupKoTu (Hai[] sorted)
		{
			int i = 1;
			while (++i < sorted.Length) {
				if (sorted [i - 2] != null &&
					sorted [i - 1] != null &&
					sorted [i] != null &&
					sorted [i - 2].Label.Equals (sorted [i].Label)) {
					var r = new KoTu (sorted [i - 2], sorted [i - 1], sorted [i]);
					sorted [i - 2] = sorted [i - 1] = sorted [i] = null;
					return r;
				}
			}
			return null;
		}

		public static SyunTu LookupSyunTu (Hai[] sorted)
		{
			int i = -1, a = -1, b = -1;
			while (++i < sorted.Length) {
				if (sorted [i] != null && sorted [i].Label is Su) {
					var f = (Su)sorted [i].Label;
					if (a == -1) {
						a = i;
					} else if (b == -1) {
						var e = (Su)sorted [a].Label;
						if (e.Type != f.Type) {
							a = -1;
						} else if (e.Number == f.Number - 1) {
							b = i;
						}
					} else {
						var e = (Su)sorted [b].Label;
						if (e.Type != f.Type) {
							a = b = -1;
						} else if (e.Number == f.Number - 1) {
							var r = new SyunTu (sorted [a], sorted [b], sorted [i]);
							sorted [a] = sorted [b] = sorted [i] = null;
							return r;
						}
					}
				}
			}
			return null;
		}
	}

	public interface IForm
	{
		//	Hai GetFinalHai ();
	}

	public interface IPair
	{
		int Length { get; }
		Hai GetHai (int index);

		// IPair(Hai[]);
	}

/*	public class NormalForm : IForm
	{
		IMentsu[] m;
		IPair[] furo;
		ToiTu? t = null;

		static NormalForm Evaluate (Hai[] hand, IPair[] furo)
		{
			return null;
		}

		Hai GetFinalHai ()
		{
			return null;
		}
	}

	public class ThirteenOrphansForm : IForm
	{
		IPair s;

		static ThirteenOrphansForm Evaluate (Hai[] hand, IPair[] furo)
		{
			if (furo.Length != 0)
				return null;
			return null;
		}
	}

	public class SevenPairsForm : IForm
	{
		ToiTu[] h;

		static SevenPairsForm Evaluate (Hai[] hand, IPair[] furo)
		{
			ToiTu? a = null;
			do {
				a = FormUtil.LookupToiTu (hand);
			} while(a != null);
			return null;
		}
	}*/

	public abstract class BasicPair : IPair
	{
		public abstract int Length { get; }

		readonly Hai[] hai;

		#region IPair implementation
		public Hai GetHai (int index)
		{
			return hai [index];
		}
		#endregion

		public BasicPair (params Hai[] hai)
		{
			if (hai.Length != Length)
				throw new InvalidProgramException ("KoTu must have 3 hais");
			this.hai = hai;
		}
		
	}

	public abstract class MenTu : BasicPair
	{
		public override int Length {
			get { return 3; }
		}

		public MenTu (Hai a, Hai b, Hai c)
			: base(a, b, c)
		{
		}
	}

	public class KoTu : MenTu
	{
		public KoTu (Hai a, Hai b, Hai c)
			: base(a, b, c)
		{
			if (a.Label == b.Label && b.Label == c.Label) {
				throw new InvalidOperationException ("the passed 3 hais are the not same.");
			}
		}
	}

	public class SyunTu : MenTu
	{
		public SyunTu (Hai a, Hai b, Hai c)
			: base(a, b, c)
		{
			if (!HaiLabelUtil.AreSequent (a.Label, b.Label, c.Label)) {
				throw new InvalidOperationException ("passed hai were not sequent: " +
					"" + a.Label + ", " + b.Label + ", " + c.Label
				);
			}
		}
	}

	public class ToiTu : BasicPair
	{
		public override int Length {
			get { return 2; }
		}

		public ToiTu (Hai a, Hai b)
			: base(new Hai[] {a, b})
		{
		}
	}
}
