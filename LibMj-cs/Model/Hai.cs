using System;
using System.Collections.Generic;
using System.Linq;

namespace LibMjcs
{
	public class Hai : IEquatable<IHaiLabel>, IComparable<Hai>
	{
		static int c = 0;
		readonly private int uuid;
		readonly private  IHaiLabel label;

		public int Uuid {
			get {
				return this.uuid;
			}
		}

		public IHaiLabel Label {
			get {
				return this.label;
			}
		}

		public Hai (IHaiLabel label)
		{
			uuid = c++;
			this.label = label;
		}

		public override bool Equals (object obj)
		{
			if (ReferenceEquals (this, obj))
				return true;
			var hai = obj as Hai;
			if (hai != null) {
				return uuid == hai.Uuid;
			} 
			return false;
		}

		public override int GetHashCode ()
		{
			unchecked {
				return uuid.GetHashCode ();
			}
		}

		public int CompareTo (Hai other)
		{
			return HaiLabelUtil.Compare (Label, other.Label);
		}

		public bool Equals (IHaiLabel other)
		{
			return other != null && other == label;
		}

		public override string ToString ()
		{
			return string.Format ("[Hai: uuid={0}, label={1}]", uuid, label);
		}
	}

	public static class HaiUtil
	{
		public static bool IsRoTo (this Hai hai)
		{
			if (!(hai.Label is Su))
				return false;
			var l = (Su)hai.Label;
			return l.Number == 9 || l.Number == 1;
		}

		public static bool IsTyunTyan (this Hai hai)
		{
			return hai.Label is Su && !hai.IsRoTo ();
		}

		public static bool IsFu (this Hai hai)
		{
			if (!(hai.Label is Tu))
				return false;
			var l = (Tu)hai.Label;
			return (int)l.Type < 4;
		}

		public static bool IsSanGen (this Hai hai)
		{
			return hai.Label is Tu && !hai.IsFu ();
		}

		public static bool IsYaoTyu (this Hai hai)
		{
			return hai.Label is Tu || hai.IsRoTo ();
		}

		#if DEBUG
		public static Hai[] Instanciate (String a)
		{
			char c2 = '-';
			var l = new List<Hai> ();
			foreach (char c in a) {
				if (Char.IsLetter (c)) {
					if (Char.IsLower (c) && c2 != '-') {
						int i = Array.FindIndex (HaiName.TuName, (s) => s [0] == c2 && s [1] == c);
						if (i != -1) {
							l.Add (new Hai (new Tu ((Tu.TuType)i)));
						}
					} else {
						c2 = c;
					}
				} else if (Char.IsNumber (c) && c2 != '-') {
					int i = Array.FindIndex (HaiName.SuTypeName, (s) => s [0] == c2);
					if (i != -1 && c != '0') {
						l.Add (new Hai (new Su ((c - '0'), (Su.SuType)i)));
					} else {
					}
				}
			}
			return l.OrderBy (s => s).ToArray ();
		}

		public static string ToString (this Hai[] h)
		{
			string a = "";
			foreach (var b in h) {
				if (b != null)
					a += b.Label;
			}
			return a;

		}

		public static void Print (this Hai[] h)
		{
			Console.WriteLine (h.ToString ());
		}
		#endif
	}
}

